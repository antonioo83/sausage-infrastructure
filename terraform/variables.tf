variable "service_account_key_file" {
  description = "Number of instances to provision."
  type        = string
  default     = "./authorized_key.json"
}

variable "cloud_id" {
  description = "Number of instances to provision."
  type        = string
  default     = "b1g3jddf4nv5e9okle7p"
}

variable "folder_id" {
  description = "Number of instances to provision."
  type        = string
  default     = "b1g6kb8sqccdk2sg5drr"
}

variable "zone" {
  description = "Number of instances to provision."
  type        = string
  default     = "ru-central1-a"
}