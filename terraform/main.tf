module "yandex_cloud_instance" {
  source = "./modules/tf-yc-instance"
  instance_zone = var.zone
  instance_subnet_id = [
    for x in module.yandex_cloud_network.yandex_vpc_subnets :
          x if x.zone == var.zone
  ][0].subnet_id
} 

module "yandex_cloud_network" {
  source = "./modules/tf-yc-network"
} 