variable "name" {
  type        = string
  default     = "chapter5-lesson2-std-012-0004"
}

variable "platform_id" {
  type        = string
  default     = "standard-v1"
}

variable "instance_zone" {
  type        = string
  default     = "ru-central1-a"
}

variable "image_id" {
  type        = string
  default     = "fd80qm01ah03dkqb14lc"
}

variable "instance_subnet_id" {
  type        = string
  default     = "e9bglr3c0r3cklccie5m"
}

variable "resource_cores" {
  type        = number
  default     = 2
}

variable "resource_memory" {
  type        = number
  default     = 2
}

variable "boot_disk_size" {
  type        = number
  default     = 50
}