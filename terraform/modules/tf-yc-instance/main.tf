resource "yandex_compute_instance" "vm" {
    name = var.name
	platform_id = var.platform_id
    zone = var.instance_zone

    resources {
        cores  = var.resource_cores
        memory = var.resource_memory
    }

    boot_disk {	    
        initialize_params {
            image_id = var.image_id
			size = var.boot_disk_size
        }
    }
	
	scheduling_policy {
      preemptible = false
    }	

    network_interface {
        subnet_id = var.instance_subnet_id
        nat       = true
    }

    metadata = {
	    user-data = "${file("./meta.txt")}"
    }		
}