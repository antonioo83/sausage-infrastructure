data "yandex_vpc_network" "default" {
  name = "default"
}

data "yandex_vpc_subnet" "default" {
  for_each = toset(["ds-test-ru-central1-c", "default-ru-central1-c", "default-ru-central1-b", "ds-test-ru-central1-b", "ds-test-ru-central1-a", "default-ru-central1-a"])
  name = "${each.key}"
} 