#! /bin/bash
set -xe
echo "${KUBE_CONFIG}" >> /var/www/infrastructure/kubernetis/kubeconfig
kubectl apply -f /var/www/infrastructure/kubernetis/backend --kubeconfig=/var/www/infrastructure/kubernetis/kubeconfig
kubectl apply -f /var/www/infrastructure/kubernetis/frontend --kubeconfig=/var/www/infrastructure/kubernetis/kubeconfig
kubectl apply -f /var/www/infrastructure/kubernetis/backend-report --kubeconfig=/var/www/infrastructure/kubernetis/kubeconfig
rm /var/www/infrastructure/kubernetis/kubeconfig